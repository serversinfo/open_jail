#include <sdktools>
#include <emitsoundany>
#include <noCTcfg>
#include <t2lrcfg>
#include <clientprefs>
#define VERSION "1.0.106"
#define GetEntityName(%1,%2,%3) GetEntPropString(%1, Prop_Data, "m_iName", %2, %3)
#define CLIENTPREFS_AVAILABLE()		(GetFeatureStatus(FeatureType_Native, "RegClientCookie") == FeatureStatus_Available)

public Plugin myinfo =
{
 name		 = "Open Jail",
 author		 = "ShaRen",
 description = "Открывает джайлы",
 version	 = VERSION
}

int g_iDoorClient;
int g_iRes_Sound[MAXPLAYERS+1];
Handle cookieResPref;
Handle kv		=	INVALID_HANDLE;
char g_sFile[256];

public void OnPluginStart()
{
	RegAdminCmd("sm_open_jail",	eV_open_jail, ADMFLAG_ROOT);
	RegAdminCmd("sm_door",		eV_door, ADMFLAG_ROOT);
	RegAdminCmd("sm_disco",		eV_disco, ADMFLAG_ROOT);
	RegConsoleCmd("sm_res", ResCmd, "On/Off Round End Sounds");
	RegConsoleCmd("sm_stop",	Command_StopSound);
	CreateTimer(0.1, ShowLookAt, _, TIMER_REPEAT);
}

public OnConfigsExecuted()
{
	if (cookieResPref == INVALID_HANDLE && CLIENTPREFS_AVAILABLE()) {
		cookieResPref = RegClientCookie("Round End Sound", "Round End Sound", CookieAccess_Private);
		SetCookieMenuItem(ResPrefSelected, 0, "Round End Sound");
	}
}


bool Doors()
{
	kv = CreateKeyValues("open_jail");
	
	BuildPath(Path_SM, g_sFile, sizeof (g_sFile), "configs/open_jail.cfg");		//причины бана
	
	if(!FileToKeyValues(kv, g_sFile))
		SetFailState("Configs not found");
	bool bSuccess;
	//g_iReasonCount = 0;
	char sMapName[64];
	GetCurrentMap(sMapName, sizeof(sMapName));
	if(KvJumpToKey(kv, "Doors")) {
		if(KvJumpToKey(kv, sMapName)) {
			if(KvGotoFirstSubKey(kv, false)) {
				do {
					char sType[16];
					char sBufer[128];
					char sEntType[64];
					char sEntName[64];
					char sEntAction[64];
					KvGetSectionName(kv, sType, sizeof(sType));
					KvGetString(kv, NULL_STRING, sBufer, sizeof(sBufer));
					//PrintToChatAll("%s %s", sType, sBufer);
					if(sType[0] == 'n') {
						int iCount;
						bool bAction;
						for(int i=0; i<sizeof(sBufer); i++)
							if(!iCount) {
								if (sBufer[i] != '|')
									sEntType[i] = sBufer[i];
								else iCount = i;
							} else if (!bAction) {
								if (sBufer[i] != '|')
									sEntName[i-iCount-1] = sBufer[i];
								else {
									bAction = true;
									iCount = i;
								}
							} else if (sBufer[i] != '\0')
								sEntAction[i-iCount-1] = sBufer[i];
							else break;

						//PrintToChatAll("DoEntByName(\"%s\", \"%s\", \"%s\")", sEntType, sEntName, sEntAction);
						DoEntByName(sEntType, sEntName, sEntAction);
						bSuccess = true;
					} else if (sType[0] == 'p' || sType[0] == 'P') {
						int iCount;
						bool bAction;
						float fPos[3];
						char sPos[12];
						int iPos;
						for(int i=0; i<sizeof(sBufer); i++)
							if(!iCount) {
								if (sBufer[i] != '|')
									sEntType[i] = sBufer[i];
								else iCount = i;
							} else if (!bAction) {
								if (iPos <3) {
									if (sBufer[i] != '|') {
										sPos[i-iCount-1] = sBufer[i];
									} else {
										fPos[iPos]= StringToFloat(sPos);
										for(int j=0;j<sizeof(sPos); j++)
											if(sPos[j] == '\0')
												break;
											else sPos[j] ='\0';
										iCount = i;
										iPos++;
									}
								} else {
									bAction = true;
									i--;
								}
							} else if (sBufer[i] != '\0')
								sEntAction[i-iCount-1] = sBufer[i];
							else break;
						DoEntByPos(sEntType, fPos, sEntAction);
						bSuccess = true;
					} //else PrintToConsole(iClient, "Undefined Type %s", sType);
					//ReplaceString(g_sBanReasonShort[g_iReasonCount], sizeof(g_sBanReasonShort), "'", "", false);
					//g_iReasonCount++;
				} while(KvGotoNextKey(kv, false));
			} else PrintToChatAll("in %s not found SubKeys", sMapName);
		} else PrintToChatAll("kv, %s not found", sMapName);
	} else PrintToChatAll("kv, Doors not found ");
	KvRewind(kv);
	return bSuccess;
}


public ResPrefSelected(client, CookieMenuAction:action, any:info, String:buffer[], maxlen)
{
	switch (action) {
		case CookieMenuAction_DisplayOption : {
			decl String:status[10];
			FormatEx(status, sizeof(status), g_iRes_Sound[client] ? "Теперь вы будете слушать музыку":"Теперь вы не будете слушать музыку");
			FormatEx(buffer, maxlen, "Музыка");
		}
		case CookieMenuAction_SelectOption : {
			switch (g_iRes_Sound[client]) {
				case 0 : g_iRes_Sound[client] = 1;
				default : g_iRes_Sound[client] = 0;
			}
			ShowCookieMenu(client);
		}
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (IsPlayerAlive(client) && buttons & IN_USE && (!IsThereCTs() || IsLrActivated())) {
		int iTarget = GetClientAimTarget(client, false);
		if (iTarget < 1)
			return Plugin_Continue;

		float fPos[3], fPosCl[3];
		GetEntPropVector(iTarget, Prop_Send, "m_vecOrigin", fPos);
		GetEntPropVector(client, Prop_Send, "m_vecOrigin", fPosCl);
		if (GetVectorDistance(fPos, fPosCl) > 100.0)
			return Plugin_Continue;
		char clsname[64];
		GetEntityClassname(iTarget, clsname, sizeof(clsname));
		if ((StrEqual(clsname, "func_door")) || StrEqual(clsname, "func_movelinear") || StrEqual(clsname, "func_door_rotating")) {
			AcceptEntityInput(iTarget, "Unlock");
			AcceptEntityInput(iTarget, "Use");
		}
	}
	return Plugin_Continue;
}

void DoEntByPos(char[] Class, float[] fPosition, char[] Input){
	char cls[32]; 
	float fPos[3];
	int i = GetEntityCount();
	while (i > MaxClients) {
		if (IsValidEntity(i)) {
			GetEntityClassname(i, cls, 32);
			if (StrEqual(cls, Class)) {
				GetEntPropVector(i, Prop_Send, "m_vecOrigin", fPos);
				if (fPos[0] == fPosition[0] && fPos[1] == fPosition[1] && fPos[2] == fPosition[2])
					AcceptEntityInput(i, Input);
			}
		}
		i--;
	}
}

public Action ShowLookAt(Handle timer)
{
	if (g_iDoorClient) {
		int iTarget = GetClientAimTarget(g_iDoorClient, false);
		if (iTarget > 0) {
			char clsname[64], name[128];
			GetEntityClassname(iTarget, clsname, sizeof(clsname));
			GetEntityName(iTarget, name, sizeof(name));
			if (name[0] != '\0') {
				char mapname[64];
				GetCurrentMap(mapname, sizeof(mapname));
				PrintHintText(g_iDoorClient, "%s (%d): \"%s\"", clsname, iTarget, name);
				PrintToConsole(g_iDoorClient, "%s (%d): \"%s\" on %s", clsname, iTarget, name, mapname);
			} else {
				float fPos[3];
				GetEntPropVector(iTarget, Prop_Send, "m_vecOrigin", fPos);
				PrintHintText(g_iDoorClient, "%s (%d): \"%.1f %.1f %.1f\"", clsname, iTarget, fPos[0], fPos[1], fPos[2]);
				PrintToConsole(g_iDoorClient, "%s (%d): \"%.1f %.1f %.1f\"", clsname, iTarget, fPos[0], fPos[1], fPos[2]);
			}
		}
	}
}

public Action Command_StopSound(int iClient, int args)
{
	if(0 < iClient <= MaxClients && IsClientInGame(iClient))
		PlaySnd(iClient, "items/flashlight1.wav");
}

public Action eV_disco(iClient, args) 
{
	CreateTimer(2.0, GetSND);
}

public Action eV_door(iClient, args) 
{
	g_iDoorClient ? (g_iDoorClient=0):(g_iDoorClient = iClient);
}

public Action GetSND(Handle timer)
{
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsClientInGame(i) && g_iRes_Sound[i])
			GetSoundFromMap(i);
}

public void OnClientPostAdminCheck(int iClient)
{
	if (!IsFakeClient(iClient)) {
		if (CLIENTPREFS_AVAILABLE())
			loadClientCookiesFor(iClient);
		CreateTimer(20.0, TimerAnnounce, GetClientSerial(iClient), TIMER_FLAG_NO_MAPCHANGE);
		GetSoundFromMap(iClient);
	}
}

public Action TimerAnnounce(Handle:timer, any:serial)
{
	int iClient = GetClientFromSerial(serial);
	if (iClient)
		PrintToChat(iClient, "Пиши в чат !res чтобы включить или отключить музыку. Или !stop чтобы останвить.");
}

public OnClientCookiesCached(iClient)
{
	if (IsClientInGame(iClient) && !IsFakeClient(iClient))
		loadClientCookiesFor(iClient);
}

loadClientCookiesFor(iClient)
{
	if (cookieResPref == INVALID_HANDLE || !AreClientCookiesCached(iClient)) {
		g_iRes_Sound[iClient] = 1;
		return;
	}
	
	decl String:buffer[5];
	GetClientCookie(iClient, cookieResPref, buffer, sizeof(buffer));
	
	if (buffer[0])
		g_iRes_Sound[iClient] = StringToInt(buffer);
	else g_iRes_Sound[iClient] = 1;
}

void PlaySnd(int iClient, char[] sPath)
{
	PrecacheSoundAny(sPath);
	if (!iClient)
		EmitSoundToAllAny(sPath, _, _, _, _, 0.8);
	else if (g_iRes_Sound[iClient])
		EmitSoundToClientAny(iClient, sPath, _, _, _, _, 0.15);
}

public Action:ResCmd(iClient, args)
{
	if (!iClient)
		return Plugin_Continue;

	if (g_iRes_Sound[iClient]) {
		g_iRes_Sound[iClient] = 0;
		PrintToChat(iClient, "Музыка отключена");
	} else {
		g_iRes_Sound[iClient] = 1;
		PrintToChat(iClient, "Музыка включена");
	}
	
	if (CLIENTPREFS_AVAILABLE()) {
		decl String:buffer[5];
		
		IntToString(g_iRes_Sound[iClient], buffer, sizeof(buffer));
		SetClientCookie(iClient, cookieResPref, buffer);
	}
	
	return Plugin_Handled;
}

void GetSoundFromMap(int iClient)
{
	char mapname[64];
	GetCurrentMap(mapname, sizeof(mapname));
	
	if(StrEqual(mapname, "jb_avalanche_csgo_b6")) {
		switch(GetRandomInt(1,12)) {
			case 1 : PlaySnd(iClient, "avalanche/supermassive_black_hole_compressed.mp3");
			case 2 : PlaySnd(iClient, "avalanche/my_generation_compressed.mp3");
			case 3 : PlaySnd(iClient, "avalanche/yellowcard_lights_and_sounds.mp3");
			case 4 : PlaySnd(iClient, "avalanche/OKGO_INVINCIBLE_compressed.mp3");
			case 5 : PlaySnd(iClient, "avalanche/tron_derezzed_compressed.mp3");
			case 6 : PlaySnd(iClient, "avalanche/supermassive_black_hole_compressed.mp3");
			case 7 : PlaySnd(iClient, "avalanche/tron_derezzed_compressed.mp3");
			case 8 : PlaySnd(iClient, "avalanche/my_generation_compressed.mp3");
			case 9 : PlaySnd(iClient, "avalanche/ascension_compressed.mp3");
			case 10 : PlaySnd(iClient, "avalanche/hbfs_short.mp3");
			case 11 : PlaySnd(iClient, "avalanche/pokemon.mp3");
			case 12 : PlaySnd(iClient, "avalanche/supertux_ice_level_compressed.mp3");
		}
	} else if(StrEqual(mapname, "jb_lego_jail_v8")) {
		switch(GetRandomInt(1,11)) {
			case 1 : PlaySnd(iClient, "jb_lego_jail/song1.mp3");
			case 2 : PlaySnd(iClient, "jb_lego_jail/song2.mp3");
			case 3 : PlaySnd(iClient, "jb_lego_jail/song3.mp3");
			case 4 : PlaySnd(iClient, "jb_lego_jail/song4.mp3");
			case 5 : PlaySnd(iClient, "aliassounds/musicalrooms/theprodigy-breath.wav");
			case 6 : PlaySnd(iClient, "aliassounds/musicalrooms/ymca.wav");
			case 7 : PlaySnd(iClient, "aliassounds/musicalrooms/gigidagostino-blablabla.wav");
			case 8 : PlaySnd(iClient, "admin_plugin/banana.mp3");
			case 9 : PlaySnd(iClient, "admin_plugin/benny1.mp3");
			case 10 : PlaySnd(iClient, "aliassounds/musicalrooms/theprodigy-firestarter.wav");
			case 11 : PlaySnd(iClient, "ba_knys_jail/musik1_infinity.mp3");
		}
	} else if(StrEqual(mapname, "jb_sides_v1a_")) {
		switch(GetRandomInt(1,3)) {
			case 1 : PlaySnd(iClient, "jb_sides/disco1.mp3");
			case 2 : PlaySnd(iClient, "jb_sides/disco2.mp3");
			case 3 : PlaySnd(iClient, "jb_sides/disco3.mp3");
		}
	} else if(StrEqual(mapname, "jb_carceris_021")) {
		switch(GetRandomInt(1,4)) {
			case 1 : PlaySnd(iClient, "daankemps/carceris/song01.mp3");
			case 2 : PlaySnd(iClient, "daankemps/carceris/song02.mp3");
			case 3 : PlaySnd(iClient, "daankemps/carceris/song03.mp3");
			case 4 : PlaySnd(iClient, "daankemps/carceris/song04.mp3");
		}
	} else if(StrEqual(mapname, "jb_summer_jail_v1")) {
		switch(GetRandomInt(1,3)) {
			case 1 : PlaySnd(iClient, "sgo1.mp3");
			case 2 : PlaySnd(iClient, "sgo2.mp3");
			case 3 : PlaySnd(iClient, "sgo3.mp3");
		}
	} else if(StrEqual(mapname, "ba_jail_campus_2011_final_csgo")) {
		switch(GetRandomInt(1,6)) {
			case 1 : PlaySnd(iClient, "campus2011/alone.mp3");
			case 2 : PlaySnd(iClient, "campus2011/quiet riot - cum on feel the noise.mp3");
			case 3 : PlaySnd(iClient, "campus2011/pinas van pinas vienen.mp3");
			case 4 : PlaySnd(iClient, "campus2011/shake.mp3");
			case 5 : PlaySnd(iClient, "campus2011/hinder - use me.mp3");
			case 6 : PlaySnd(iClient, "campus2011/kernkraft 400 - zombie nation.mp3");
		}
	} else if(StrEqual(mapname, "jb_vipinthemix_csgo_v1-1")) {
		switch(GetRandomInt(1,7)) {
			case 1 : PlaySnd(iClient, "inthemix/deadmau5 - ghost 'n stuff.mp3");
			case 2 : PlaySnd(iClient, "inthemix/deadmau5 - strobe.mp3");
			case 3 : PlaySnd(iClient, "inthemix/afrojack - take over control.mp3");
			case 4 : PlaySnd(iClient, "inthemix/usher - more.mp3");
			case 5 : PlaySnd(iClient, "inthemix/martin solveig - hello.mp3");
			case 6 : PlaySnd(iClient, "inthemix/david guetta - who's that chick.mp3");
			case 7 : PlaySnd(iClient, "inthemix/showtek - we speak music.mp3");
		}
	} else if(StrEqual(mapname, "jb_mountaincraft_v5")) {
		switch(GetRandomInt(1,2)) {
			case 1 : PlaySnd(iClient, "minecraft sound/the cha cha slide - dj casper (lyrics) by lyndsie15.mp3");
			case 2 : PlaySnd(iClient, "minecraft sound/the glitch mob - our demons (feat. aja volkman).mp3");
		}
	} else if(StrEqual(mapname, "jb_mountaincraft_v6"))
		PlaySnd(iClient, "minecraft sound/the glitch mob - our demons (feat. aja volkman).mp3");

	else if(StrEqual(mapname, "ba_jail_minecraftparty_v6")) {
		switch(GetRandomInt(1,4)) {
			case 1 : PlaySnd(iClient, "minecraftparty/dwarfhole.mp3");
			case 2 : PlaySnd(iClient, "minecraftparty/minecraftstyle.mp3");
			case 3 : PlaySnd(iClient, "minecraftparty/et.mp3");
			case 4 : PlaySnd(iClient, "minecraftparty/revenge.mp3");
		}
	} else if(StrEqual(mapname, "ba_jail_blackops_v2")) {
		switch(GetRandomInt(1,6)) {
			case 1 : PlaySnd(iClient, "disco/jeopardy.mp3");
			case 2 : PlaySnd(iClient, "disco/pocket.mp3");
			case 3 : PlaySnd(iClient, "disco/moveyourbody.mp3");
			case 4 : PlaySnd(iClient, "disco/kickstart.mp3");
			case 5 : PlaySnd(iClient, "disco/radiogg.mp3");
			case 6 : PlaySnd(iClient, "disco/madeon.mp3");
		}
	} else if(StrEqual(mapname, "ba_jail_rebellion")) {
		switch(GetRandomInt(1,3)) {
			case 1 : PlaySnd(iClient, "sawk/bi_music_1.wav");
			case 2 : PlaySnd(iClient, "sawk/bi_music_2.wav");
			case 3 : PlaySnd(iClient, "sawk/bi_music_3.wav");
		}
	} else if(StrEqual(mapname, "jb_adrenalin_neon_v1")) {
		switch(GetRandomInt(1,3)) {
			case 1 : PlaySnd(iClient, "davidisadick/corona - rhythm of the night.mp3");
			case 2 : PlaySnd(iClient, "davidisadick/vanic x machineheart - circles.mp3");
			case 3 : PlaySnd(iClient, "davidisadick/we love video games! - inside gaming music video.mp3");
		}
	} else if(StrEqual(mapname, "jb_mangaroons_bh")) {
		switch(GetRandomInt(1,4)) {
			case 1 : PlaySnd(iClient, "jb_song_colours_new.mp3");
			case 2 : PlaySnd(iClient, "jb_song_4.mp3");
			case 3 : PlaySnd(iClient, "jb_song_5.mp3");
			case 4 : PlaySnd(iClient, "jb_song_3_new.mp3");
		}
	} else if(StrEqual(mapname, "jb_obama_v5_beta")) {
		switch(GetRandomInt(1,7)) {
			case 1 : PlaySnd(iClient, "custom/aqua - barbie girl-[www_flvto_com].mp3");
			case 2 : PlaySnd(iClient, "custom/psy - gangnam style.mp3");
			case 3 : PlaySnd(iClient, "custom/electric six - _gay bar_ (hi res)-[www_flvto_com].mp3");
			case 4 : PlaySnd(iClient, "custom/house of pain jump around, lyrics in the description and mp3 download-[www_flvto_com].mp3");
			case 5 : PlaySnd(iClient, "custom/lmfao - sexy and i know it-[www_flvto_com].mp3");
			case 6 : PlaySnd(iClient, "custom/carly rae jepsen call me maybe.mp3");
			case 7 : PlaySnd(iClient, "custom/screw the nether (moves like jagger parody).mp3");
		}
	} //else if(StrEqual(mapname, "jb_spy_vs_spy_beta7_6")) {
	//	switch(GetRandomInt(1,5)) {
	//		case 1 : PlaySnd(iClient, "spyvsspy/spy vs spy xbox music.wav");
	//		case 2 : PlaySnd(iClient, "spyvsspy/spy vs spy - c64.wav");
	//		case 3 : PlaySnd(iClient, "spyvsspy/spy vs spy mansion level.wav");
	//		case 4 : PlaySnd(iClient, "spyvsspy/spy vs spy nes.wav");
	//		case 5 : PlaySnd(iClient, "spyvsspy/spy vs spy robot factory level.wav");
	//	}
	//}
	else if(StrEqual(mapname, "jb_dust2")) {
		switch(GetRandomInt(1,10)) {
			case 1 : PlaySnd(iClient, "jb_dust2/boxe_d2.wav");
			case 2 : PlaySnd(iClient, "jb_dust2/corda_d2.wav");
			case 3 : PlaySnd(iClient, "jb_dust2/music5_d2.wav");
			case 4 : PlaySnd(iClient, "jb_dust2/music1_d2.wav");
			case 5 : PlaySnd(iClient, "jb_dust2/music4_d2.wav");
			case 6 : PlaySnd(iClient, "jb_dust2/music3_d2.wav");
			case 7 : PlaySnd(iClient, "jb_dust2/music2_d2.wav");
			case 8 : PlaySnd(iClient, "jb_dust2/tv_d2.wav");
			case 9 : PlaySnd(iClient, "jb_dust2/ss_dr.wav");
			case 10 : PlaySnd(iClient, "jb_dust2/ss_kz.wav");
		}
	} else if(StrEqual(mapname, "jb_dust2_final2")) {
		switch(GetRandomInt(1,8)) {
			case 1 : PlaySnd(iClient, "jb_dust2/boxe_d2.wav");
			case 2 : PlaySnd(iClient, "jb_dust2/corda_d2.wav");
			case 3 : PlaySnd(iClient, "jb_dust2/music5_d2.wav");
			case 4 : PlaySnd(iClient, "jb_dust2/music1_d2.wav");
			case 5 : PlaySnd(iClient, "jb_dust2/music4_d2.wav");
			case 6 : PlaySnd(iClient, "jb_dust2/music3_d2.wav");
			case 7 : PlaySnd(iClient, "jb_dust2/music2_d2.wav");
			case 8 : PlaySnd(iClient, "jb_dust2/tv_d2.wav");
		}
	} else if(StrEqual(mapname, "jb_jailberd_v5_final_fix")) {
		switch(GetRandomInt(1,10)) {
			case 1 : PlaySnd(iClient, "jb_jailberd/daft punk - contact.mp3");
			case 2 : PlaySnd(iClient, "jb_jailberd/13. never le nkemise 2.mp3");
			case 3 : PlaySnd(iClient, "jb_jailberd/fatboy slim & riva starr ft. beardyman - eat, sleep, rave, repeat (lyric video).mp3");
			case 4 : PlaySnd(iClient, "jb_jailberd/birdy nam nam.mp3");
			case 5 : PlaySnd(iClient, "jb_jailberd/crizzly - lifted.mp3");
			case 6 : PlaySnd(iClient, "jb_jailberd/lo lindora.mp3");
			case 7 : PlaySnd(iClient, "jb_jailberd/2 many dicks.mp3");
			case 8 : PlaySnd(iClient, "jb_jailberd/heyyeyaaeyaaaeyaeyaa_(360p).mp3");
			case 9 : PlaySnd(iClient, "jb_dust2/ss_dr.wav");
			case 10 : PlaySnd(iClient, "jb_dust2/ss_kz.wav");
		}
	}
	//else if(StrEqual(mapname, "jb_exile2019_v1e")) {
	//	switch(GetRandomInt(1,17)) {
	//		case 1 : PlaySnd(iClient, "music/coloursjumparound.mp3");
	//		case 2 : PlaySnd(iClient, "music/catchcustom/jb_discosong1.mp3");
	//		case 3 : PlaySnd(iClient, "music/catchcustom/jb_discosong2.mp3");
	//		case 4 : PlaySnd(iClient, "music/catchcustom/jb_discosong3.mp3");
	//		case 5 : PlaySnd(iClient, "music/catchcustom/redgreen_song.mp3");
	//		case 6 : PlaySnd(iClient, "music/musical_chairs/mcsong_1.mp3");
	//		case 7 : PlaySnd(iClient, "music/musical_chairs/mcsong_10.mp3");
	//		case 8 : PlaySnd(iClient, "music/musical_chairs/mcsong_2.mp3");
	//		case 9 : PlaySnd(iClient, "music/musical_chairs/mcsong_3.mp3");
	//		case 10 : PlaySnd(iClient, "music/musical_chairs/mcsong_4.mp3");
	//		case 11 : PlaySnd(iClient, "music/musical_chairs/mcsong_5.mp3");
	//		case 12 : PlaySnd(iClient, "music/musical_chairs/mcsong_6.mp3");
	//		case 13 : PlaySnd(iClient, "music/musical_chairs/mcsong_7.mp3");
	//		case 14 : PlaySnd(iClient, "music/musical_chairs/mcsong_8.mp3");
	//		case 15 : PlaySnd(iClient, "music/musical_chairs/mcsong_9.mp3");
	//		case 16 : PlaySnd(iClient, "music/catchcustom/tearingmeapartlisa1.mp3");
	//		case 17 : PlaySnd(iClient, "music/catchcustom/plebsonjailbreak.mp3");
	//	}
	//}
	else if(StrEqual(mapname, "jb_castleguarddev_v5")) {
		switch(GetRandomInt(1,2)) {
			case 1 : PlaySnd(iClient, "castleguarddevdisco/the glitch mob - we can make the world stop.mp3");
			case 2 : PlaySnd(iClient, "castleguarddevdisco/the trammps - disco inferno.mp3");
		}
	}
}

public Action eV_open_jail(int client, int args)
{
	if (Doors())
		return Plugin_Continue;

	char mapname[64];
	GetCurrentMap(mapname, sizeof(mapname));

	if(StrEqual(mapname, "ba_jail_electric_vip_SI")) {
		DoEntByName("func_door", "cell_door_", "open");
		DoEntByName("func_door", "colitary_door", "open");
	}
	else if(StrEqual(mapname, "ba_jail_ascention"))
		DoEntByName("func_movelinear", "wall", "open");
		
	else if(StrEqual(mapname, "ba_jail_sona_sf_v2"))
		DoEntByName("func_door", "Cell Door", "open");

	else if(StrEqual(mapname, "jb_battleforce_xmas_csgo"))
		DoEntByName("func_door", "jail.mrize", "open");
		
	else if(StrEqual(mapname, "ba_jail_blackops_v2"))
		DoEntByName("func_door", "celldoors", "open");
		
	else if(StrEqual(mapname, "jb_snow_v2"))
		DoEntByName("func_door", "jails", "open");

	else if(StrEqual(mapname, "jb_mountaincraft_v6")) {
		DoEntByName("func_door_rotating", "cell_door_1", "open");
		DoEntByName("func_door_rotating", "cell_door_2", "open");
		DoEntByName("func_door_rotating", "cell_door_3", "open");
		DoEntByName("func_door_rotating", "cell_door_4", "open");
		DoEntByName("func_door_rotating", "cell_door_5", "open");
		DoEntByName("func_door_rotating", "cell_door_6", "open");
		DoEntByName("func_door_rotating", "cell_door_7", "open");
		DoEntByName("func_door_rotating", "cell_door_8", "open");
		DoEntByName("func_door_rotating", "cell_door_9", "open");
		DoEntByName("func_door_rotating", "cell_door_10", "open");
	}
	else if(StrEqual(mapname, "jb_nm_kistroll_b1b")) {
		float fPos[3] = {90.01, -532.5, 178.3};
		DoEntByPos("func_button", fPos, "use");
	}
	else if(StrEqual(mapname, "jb_cavern_v1b")) {
		float fPos[3] = {404.0, 2332.0, 418.0};
		DoEntByPos("func_button", fPos, "use");
	}
	else if(StrEqual(mapname, "jb_vipinthemix_csgo_v1-1")) {
		DoEntByName("func_movelinear", "Jaildoor_clip1", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip2", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip3", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip4", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip5", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip6", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip7", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip8", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip9", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip10", "open");
		DoEntByName("func_door", "Vipcel_door", "open");
		DoEntByName("func_movelinear", "Iso_door", "open");
	}
	else if(StrEqual(mapname, "ba_jail_canyondam_go")) {
		DoEntByName("func_movelinear", "CellDoors", "open");
		DoEntByName("func_movelinear", "CellDoors2", "open");
	}
	else if(StrEqual(mapname, "jb_mist_v1"))
		DoEntByName("func_breakable", "celldoor", "kill");
		
	else if(StrEqual(mapname, "jb_uzay_istasyonu_v1"))
		DoEntByName("func_door", "hucreee", "open");
	
	else if(StrEqual(mapname, "jb_exile2019_v1e")) {
		DoEntByName("func_wall_toggle", "Cell Area Door", "Toggle");
		DoEntByName("env_laser", "Cell Area Lasers", "Toggle");
		DoEntByName("func_button", "Cell Door Open", "use");
	}
	else if(StrEqual(mapname, "jb_mangaroons_bh"))
		DoEntByName("func_door", "Cell_Doors", "open");
		
	else if(StrEqual(mapname, "jb_dust2") || StrEqual(mapname, "jb_dust2_final2"))
		DoEntByName("func_door", "cela", "open");

	else if(StrEqual(mapname, "jb_br_amazonia_v3")) {
		DoEntByName("func_door", "Jail1", "open");
		DoEntByName("func_door", "Jail2", "open");
		DoEntByName("func_door", "Jail3", "open");
		DoEntByName("func_door", "Jail4", "open");
		DoEntByName("func_door", "Jail5", "open");
		DoEntByName("func_door", "Jail6", "open");
		DoEntByName("func_door", "Jail7", "open");
		DoEntByName("func_door", "Jail8", "open");
		DoEntByName("func_door", "Jail9", "open");
	}
	else if(StrEqual(mapname, "ba_arizona_2_2") || StrEqual(mapname, "jb_adrenalin_neon_v1"))
		DoEntByName("func_door", "celldoor", "open");
	
	else if(StrEqual(mapname, "jb_sides_v1a_"))
		DoEntByName("func_door", "cells1", "open");
	
	else if(StrEqual(mapname, "jb_coast_v2b"))
		DoEntByName("func_door", "jail_mrize", "open");
	
	else if(StrEqual(mapname, "jb_Servers_Info_alpha5") ||StrEqual(mapname, "jb_Servers_Info_alpha6") ||StrEqual(mapname, "jb_Servers_Info_alpha7") ||StrEqual(mapname, "jb_Servers_Info_alpha8")) {
		DoEntByName("func_door", "cell_door_", "open");
		DoEntByName("func_door", "cell_door", "open");
		DoEntByName("func_door", "colitary_door", "open");
	}
	else if(StrEqual(mapname, "jb_electric_todestrakt_v26")
		|| StrEqual(mapname, "ba_jail_electric_vip_v3")
		|| StrEqual(mapname, "jb_rehab_f1")
		|| StrEqual(mapname, "ba_jail_electric_razor_go")
		|| StrEqual(mapname, "ba_jail_electric_vip_csgo")
		|| StrEqual(mapname, "ba_jail_electric_vip_v2"))
	{
		DoEntByName("func_door", "cell_door", "open");
		DoEntByName("func_door", "colitary_door", "open");
	}
	else if(StrEqual(mapname, "ba_jail_chernobyl_day")) {
		DoEntByName("func_movelinear", "dvere_prave", "open");
		DoEntByName("func_movelinear", "dvere_leve", "open");
	}
	else if(StrEqual(mapname, "jb_castleguarddev_v5"))
		DoEntByName("func_door", "cell_door_1", "open");
	
	else if(StrEqual(mapname, "jb_clouds_final5"))
		DoEntByName("func_button", "zelle_button", "use");
	
	else if(StrEqual(mapname, "jb_jailberd_v5_final_fix"))
		DoEntByName("func_door", "Celldoor", "open");
	
	else if(StrEqual(mapname, "ba_jail_futurouta"))
		DoEntByName("func_button", "jailbutton", "use");
	
	else if(StrEqual(mapname, "jb_renegade_v3") || StrEqual(mapname, "ba_jail_rebellion") || StrEqual(mapname, "jb_spy_vs_spy_beta7_6"))
		DoEntByName("func_door", "cell_door", "open");
	
	else if(StrEqual(mapname, "ba_jail_minecraftparty_v6"))
		DoEntByName("func_button", "knopf_jail_green", "use");
	
	else if(StrEqual(mapname, "ba_extreme_jail"))
		DoEntByName("func_door", "jail1", "open");
		
	else if(StrEqual(mapname, "jb_avalanche_csgo_b6")) {
		float fPos[3] = {960.0, -128.5, 144.0};
		DoEntByPos("func_button", fPos, "use");
	}
	
	//else if(StrEqual(mapname, "ba_hs_jail_2_icy"))
	//	DoEntByPos("sm_trigger_mapentity func_door 1 open");
	
	else if(StrEqual(mapname, "ba_jail_blackops"))
		DoEntByName("func_door", "prisondoor", "open");
	
	else if(StrEqual(mapname, "ba_jail_campus_2011_final_csgo"))
		DoEntByName("func_door", "cells", "open");
	
	else if(StrEqual(mapname, "ba_jail_canyondam_v4"))
		DoEntByName("func_button", "Cell_doors_openbutton", "use");
	
	else if(StrEqual(mapname, "Ba_Jail_Canyondam_v4_Fix"))
		DoEntByName("func_button", "Cell_doors_openbutton", "use");
	
	else if(StrEqual(mapname, "ba_jail_desert"))
		DoEntByName("func_door", "celldoor", "open");
		
	else if(StrEqual(mapname, "ba_jail_goduk")) {
		DoEntByName("func_door", "doors1", "open");
		DoEntByName("func_door", "doors2", "open");
	}
	else if(StrEqual(mapname, "ba_jail_lockdown_final"))
		DoEntByName("func_button", "Cell_Open_Button", "use");
	
	else if(StrEqual(mapname, "ba_jail_lunar_mkIIf"))
		DoEntByName("func_door", "cell_sliders", "open");
	
	else if(StrEqual(mapname, "ba_jail_mars"))
		DoEntByName("func_door", "door_jail", "open");
	
	else if(StrEqual(mapname, "ba_jail_mars_csgo"))
		DoEntByName("func_door", "door_jail", "open");
	
	else if(StrEqual(mapname, "ba_jail_mars_new"))
		DoEntByName("func_door", "door_jail", "open");
	
	else if(StrEqual(mapname, "ba_jail_mars_2213"))
		DoEntByName("func_button", "cell_doors_button", "use");
	
	else if(StrEqual(mapname, "ba_jail_Bravo"))
		DoEntByName("func_door", "door_cell", "open");
	
	else if(StrEqual(mapname, "ba_dreamjail_watchtower_v2-1")) {
		DoEntByName("func_door", "jaildoor", "open");
		DoEntByName("func_door", "isodoor", "open");
	}
	else if(StrEqual(mapname, "ba_jail_mini")) {
		DoEntByName("func_door", "jdown", "open");
		DoEntByName("func_door", "jup", "open");
	}
	else if(StrEqual(mapname, "ba_jail_mini_final")) {
		DoEntByName("func_door", "tup2", "open");
		DoEntByName("func_door", "tup", "open");
	}
	else if(StrEqual(mapname, "ba_jail_sand_v3"))
		DoEntByName("func_door", "JailDoors", "open");
	
	else if(StrEqual(mapname, "ba_jail_sand_final_beta2"))
		DoEntByName ("func_door", "jaildoors", "open");
	
	else if(StrEqual(mapname, "jb_august"))
		DoEntByName("func_door", "celldoor1", "open");
	
	else if(StrEqual(mapname, "ba_jail_voodoo_final")) {
		DoEntByName("func_movelinear", "cell", "open");
		DoEntByName("func_brush", "fo1", "disable");
	}
	else if(StrEqual(mapname, "ba_jailbreak_rectangle"))
		DoEntByName("func_door", "hucre_door", "open");
	
	else if(StrEqual(mapname, "ba_jail_simpsons_final_v3")) {
		DoEntByName("func_movelinear", "jail1", "open");
		DoEntByName("func_movelinear", "jail2", "open");
		DoEntByName("func_movelinear", "jail3", "open");
		DoEntByName("func_movelinear", "jail5", "open");
		DoEntByName("func_movelinear", "jail6", "open");
		DoEntByName("func_movelinear", "jail7", "open");
		DoEntByName("func_movelinear", "jail8", "open");
		DoEntByName("func_movelinear", "jail9", "open");
	}
	else if(StrEqual(mapname, "ba_jail_Summer"))
		DoEntByName("func_door", "celldoor", "open");
	
	else if(StrEqual(mapname, "ba_jail_towers2"))
		DoEntByName("func_movelinear", "wall", "open");
	
	else if(StrEqual(mapname, "jb_battleforce_xmas_v1"))
		DoEntByName("func_door", "jail.mrize", "open");
	
	else if(StrEqual(mapname, "jb_lego_jail_pre_v6-2"))
		DoEntByName("func_door", "c1", "open");
	
	else if(StrEqual(mapname, "jb_lego_jail_v8"))
		DoEntByName("func_door", "c1", "open");
	
	else if(StrEqual(mapname, "jb_new_summer_v2"))
		DoEntByName("func_door", "cells", "open");
	
	else if(StrEqual(mapname, "jb_new_summer_v2"))
		DoEntByName("func_door", "cells", "open");
	
	else if(StrEqual(mapname, "jb_summer_jail_v1"))
		DoEntByName("func_door", "cells", "open");
	
	else if(StrEqual(mapname, "jb_obama_v5_beta"))
		DoEntByName("func_door", "cell_doors", "open");
	
	else if(StrEqual(mapname, "jb_space_jail_v1_fix2")) {
		DoEntByName("func_door", "cell1", "open");
		DoEntByName("func_door", "cell4", "open");
	}
	else if(StrEqual(mapname, "jb_vipinthemix_v1_2")) {
		DoEntByName("func_movelinear", "Jaildoor_clip1", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip2", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip3", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip4", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip5", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip6", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip7", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip8", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip9", "open");
		DoEntByName("func_movelinear", "Jaildoor_clip10", "open");
	}
	else if(StrEqual(mapname, "jb_new_chernobyl")) {
		DoEntByName("func_movelinear", "dvere_prave", "open");
		DoEntByName("func_movelinear", "dvere_lave", "open");
	} else {
		char class[32]; 
		int ent = GetMaxEntities();
		while (ent > MaxClients) {
			if (IsValidEntity(ent) && GetEntityClassname(ent, class, 32) && (StrContains(class, "_door") || !strcmp(class, "func_movelinear"))) {
				AcceptEntityInput(ent, "Unlock");
				AcceptEntityInput(ent, "Open");
			}
			ent--;
		}
		DoEntByName("func_door", "cell_door", "open");
		char szMap[50];
		GetCurrentMap(szMap, sizeof szMap);
		PrintToChatAll("Внимание, на карте %s не отмечены двери джайла, поэтому открываются все двери", szMap);
		PrintToChatAll("Если вы видите это сообщение сообщите об этом тех. админам");
	}
	return Plugin_Continue;
}

void DoEntByName(char[] Class, char[] Name, char[] Input){
	char cls[32]; 
	int i = GetEntityCount();
	char Namebuf[32];
	while (i > MaxClients) {
		Namebuf[0] = '\0';
		if (IsValidEntity(i)) {
			GetEntityClassname(i, cls, 32);
			if (StrEqual(cls, Class)) {
				GetEntPropString(i, Prop_Data, "m_iName", Namebuf, sizeof(Namebuf));
				if (StrEqual(Namebuf, Name))
					AcceptEntityInput(i, Input);
			}
		}
		i--;
	}
}